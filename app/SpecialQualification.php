<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SpecialQualification extends Model
{
    use SoftDeletes;

    public function doctors(){

        return $this->belongsToMany('App\Doctor', 'doctor_specialQualification', 'doctor_id', 'specialqualification_id');
        
         
    }
}