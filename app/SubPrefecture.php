<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubPrefecture extends Model
{
    public function prefecture()
    {
        return $this->belongsTo('App\prefecture', 'prefecture_id');
    }

    public function doctors()
    {
        return $this->hasMany('App\Doctor', 'sub_prefecture_id');
    }
}
