<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prefecture extends Model
{
    public function subprefectures()
    {
        return $this->hasMany('App\SubPrefecture', 'prefecture_id');
    }

    public function doctors()
    {
        return $this->hasMany('App\Doctor', 'prefecture_id');
    }


}
