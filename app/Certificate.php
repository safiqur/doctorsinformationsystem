<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Certificate extends Model
{
    use SoftDeletes;
    public function doctor()
    {
        return $this->belongsTo('App\Doctor', 'doctor_id');
    }
}
