<?php

namespace App\Http\Controllers;

use Mail;
use Auth;
use App\Doctor;
use App\Prefecture;
use App\Speciality;
use App\SpecialQualification;
use App\SubPrefecture;
use App\Certificate;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Boolean;
use \App\Http\Requests\StorePostRequest;
use Intervention\Image\ImageManagerStatic as Image;
use Laravel\Scout\Searchable;

class DoctorController extends Controller
{
    public function welcome()
    {

        return view('welcome');
    }

    public function previewSingleDoctor($id)
    {
        $user =  Auth::user();
        if (!$user) {
            return redirect('/');
        }
        $doctor = Doctor::where('id', $id)->first();;

        if ($doctor) {
            return view('doctorprofile', ['doctor' => $doctor]);
        } else {
            return view('welcome');
        }
        // return view('doctorprofile', ['doctor' => $doctor]);
    }

    public function getDoctorlist()
    {

        $user =  Auth::user();
        if (!$user) {
            return redirect('/');
        }

        $doctors = Doctor::all();
        if ($doctors) {
            return view('doctorprofilelist', ['doctors' => $doctors]);
        } else {
            return view('welcome');
        }
    }

    public function searchDoctorProfile(Request $request){
        // Get the search value from the request
        $search = $request->input('searchkeyword');
    
        $doctors = Doctor::query()
            ->whereHas('prefecture',function(\Illuminate\Database\Eloquent\Builder $query) use ($search){
                return $query->where('prefectures.prefecture_name', 'LIKE', "%{$search}%");
            })
            ->orwhereHas('subprefecture',function(\Illuminate\Database\Eloquent\Builder $query) use ($search){
                return $query->where('sub_prefectures.subprefecture_name', 'LIKE', "%{$search}%");
            })
            ->orWhere('first_name', 'LIKE', "%{$search}%")
            ->orWhere('last_name', 'LIKE', "%{$search}%")
            ->orWhere('email_id', 'LIKE', "%{$search}%")
            ->orWhere('registration_number', 'LIKE', "%{$search}%")
            ->get();
    

        return view('doctorprofilelist', ['doctors' => $doctors]);
    }

    public function GetSubprefactureByPrefacture($id)
    {

        $user =  Auth::user();
        if (!$user) {
            return redirect('/');
        }

        $subPrefectures = SubPrefecture::all()
            ->where("prefecture_id", $id)
            ->pluck('subprefecture_name', 'id');
        return json_encode($subPrefectures);
    }



    public function createDoctorProfile()
    {
        $user =  Auth::user();
        if (!$user) {
            return redirect('/');
        }
        //$doctor = Doctor::find($id);
        $prefectures = Prefecture::all();
        $subPrefectures = SubPrefecture::all();
        $specialities = Speciality::all();
        $specialqualifications = SpecialQualification::all();
        return view('doctorprofilecreate', ['prefectures' => $prefectures, 'subPrefectures' => $subPrefectures,   'specialities' => $specialities, 'specialqualifications' => $specialqualifications]);

        }

    public function  SaveDoctorProfile(Request $request)
    {

        //$validated = $request->validated();    
        $this->validate($request, [
            'first_name' => 'required|max:10',
            'last_name' => 'required|max:10',
            'email_id' => 'required|max:50|email|unique:doctors',
            'registration_number' => 'required|max:10|unique:doctors',
            'dateofbirth' => 'required|date',
            'start_time' => 'required',
            'end_time' => 'required',
            'prefacture' => 'required|not_in:0',
            'subprefacture' => 'required|not_in:0',
            'speciality' => 'required',
            'address' => 'required',
            'html_code' => 'required',
            'profile_pic' => 'required',
            'certificates' => 'required',
            'specialqualifications' => 'required'
        ]);

        $doctorprofile = new Doctor();
        $doctorprofile->first_name = $request->input('first_name');
        $doctorprofile->last_name = $request->input('last_name');
        $doctorprofile->email_id = $request->input('email_id');
        $doctorprofile->registration_number = $request->input('registration_number');
        if ($request->hasFile('profile_pic') && $request->file('profile_pic')->isValid()) {
            $file = $request->file('profile_pic');
            $file_name = str_random(10) . '.' . $file->getClientOriginalExtension();
            $file->move(public_path() . '/images', $file_name);
            $doctorprofile->photo =  $file_name;
        } else {
            $doctorprofile->photo = "";
        }

        $doctorprofile->date_of_birth = $request->input('dateofbirth');
        $doctorprofile->visit_start_time = $request->input('start_time');
        $doctorprofile->visit_end_time = $request->input('end_time');
        $doctorprofile->prefecture_id = $request->input('prefacture');
        $doctorprofile->sub_prefecture_id = $request->input('subprefacture');
        $doctorprofile->speciality_id = $request->input('speciality');
        $doctorprofile->address = $request->input('address');
        $doctorprofile->html_code = $request->input('html_code');

        $doctorprofile->save();
        $doctor_id = $doctorprofile->id;

        $Certificates = new Certificate();
        if ($request->hasFile('certificates') && $request->file('certificates')->isValid()) {
            $file = $request->file('certificates');
            $file_name = $doctor_id . '_' . str_random(5) . '.' . $file->getClientOriginalExtension();
            $file->move(public_path() . '/cert', $file_name);
            $Certificates->file_name  =  $file_name;
        } else {
            $Certificates->file_name  = "";
        }

        //$Certificates->file_name = $request->file('certificates')->getClientOriginalName();
        $doctorprofile->certificates()->save($Certificates);
        $doctorprofile->specialqualifications()->attach($request->input('specialqualifications') === null ? [] : $request->input('specialqualifications'));

        return redirect()->route('doctorprofilelist')->with('info', 'Doctor Profile Created');;
    }

    public function EditDoctorProfile($id)
    {
        $user =  Auth::user();
        if (!$user) {
            return redirect('/');
        }

        $doctor = Doctor::find($id);
        $prefectures = Prefecture::all();
        $subPrefectures = SubPrefecture::all();
        $specialities = Speciality::all();
        $specialqualifications = SpecialQualification::all();
        return view('doctorprofileEdit', ['doctor' => $doctor, 'id' => $id, 'prefectures' => $prefectures, 'subPrefectures' => $subPrefectures,   'specialities' => $specialities, 'specialqualifications' => $specialqualifications]);
    }

    public function  UpdateDoctorProfile(Request $request, $id)
    {

        $user =  Auth::user();
        if (!$user) {
            return redirect('/');
        }
        $this->validate($request, [
            'first_name' => 'required|max:10',
            'last_name' => 'required|max:10',
            'dateofbirth' => 'required|date',
            'start_time' => 'required',
            'end_time' => 'required',
            'prefacture' => 'required|not_in:0',
            'subprefacture' => 'required|not_in:0',
            'speciality' => 'required',
            'address' => 'required',
            'html_code' => 'required',
            'profile_pic' => 'required',
            'certificates' => 'required',
            'specialqualifications' => 'required'
        ]);


        $doctorprofile = Doctor::find($id);
        if (!$doctorprofile) {
            return redirect('/');
        }

        $doctorprofile->first_name = $request->input('first_name');
        $doctorprofile->last_name = $request->input('last_name');

        if ($request->hasFile('profile_pic') && $request->file('profile_pic')->isValid()) {
            $image       = $request->file('profile_pic');
            $filename    = str_random(10) . '.' . $image->getClientOriginalExtension();    
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(300, 300);
            $image_resize->save(public_path('images/' .$filename));
            $doctorprofile->photo =  $filename;
        } else {
            $doctorprofile->photo = "not found";
        }
        
        $doctorprofile->date_of_birth = $request->input('dateofbirth');
        $doctorprofile->visit_start_time = $request->input('start_time');
        $doctorprofile->visit_end_time = $request->input('end_time');
        $doctorprofile->prefecture_id = $request->input('prefacture');
        $doctorprofile->sub_prefecture_id = $request->input('subprefacture');
        $doctorprofile->speciality_id = $request->input('speciality');
        $doctorprofile->address = $request->input('address');
        $doctorprofile->html_code = $request->input('html_code');

        $doctorprofile->save();
        $doctor_id = $doctorprofile->id;

        $Certificates = new Certificate();
        if ($request->hasFile('certificates') && $request->file('certificates')->isValid()) {
            $file = $request->file('certificates');
            $file_name = $doctor_id . '_' . str_random(5) . '.' . $file->getClientOriginalExtension();
            $file->move(public_path() . '/cert', $file_name);
            $Certificates->file_name  =  $file_name;
        } else {
            $Certificates->file_name  = "not found";
        }


        $doctorprofile->certificates()->delete();
        $doctorprofile->certificates()->save($Certificates);
        $doctorprofile->specialqualifications()->sync($request->input('specialqualifications') === null ? [] : $request->input('specialqualifications'));

        return redirect()->route('doctorprofilelist')->with('info', 'Doctor Profile Updated');;
    }



    public function DeleteDoctorProfile($id)
    {

        $user =  Auth::user();
        if (!$user) {
            return redirect('/');
        }

        $doctorprofile = Doctor::find($id);  

        $doctorprofile->certificates()->delete();
        $doctorprofile->specialqualifications()->detach();
        $doctorprofile->delete();

        return redirect()->route('doctorprofilelist')->with('info', 'Doctor Profile Deleted');

    }
}
