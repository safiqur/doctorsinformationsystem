<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Doctor extends Model
{
    use SoftDeletes;
    
    public function certificates()
    {
        return $this->hasMany('App\Certificate', 'doctor_id');
    }

    public function speciality(){

        return $this->belongsTo('App\Speciality', 'speciality_id');
    }
    public function prefecture(){

        return $this->belongsTo('App\Prefecture', 'prefecture_id');
    }

    public function subprefecture(){

        return $this->belongsTo('App\SubPrefecture', 'sub_prefecture_id');
    }


#[SearchUsingFullText(['first_name', 'last_name','email_id','registration_number'])]
public function toSearchableArray()
{
    return [
         'first_name' => $this->first_name,
         'last_name' => $this->last_name,
         'email_id' => $this->email_id,
         'registration_number' => $this->registration_number,

    ];
}

    public function specialQualifications(){

        return $this->belongsToMany('App\SpecialQualification', 'doctor_special_qualifications', 'doctor_id', 'specialqualification_id');
         
    }

    

}
