<?php

use Illuminate\Database\Seeder;

class DoctorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Doctor = new \App\Doctor([
            'first_name' => 'Syed',
            'last_name' => 'Makhdum',
            'email_id' => 'makhdum@edoctor.jp',
            'registration_number' => 'ED001',
            'date_of_birth' => '1990-01-15',
            'prefecture_id' => '1',
            'sub_prefecture_id' => '1',
            'visit_start_time' => '15:30:00',
            'visit_end_time' => '16:30:00',
            'speciality_id' => '1',
            'photo' => 'ED001.jpeg',
            'address' => 'Dhaka, Bangladesh',
            'html_code' => '<html><h1>My Name is Syed Makhdum</h1></html>'
    
        ]);
        $Doctor->save();

        $Doctor = new \App\Doctor([
            'first_name' => 'Noor',
            'last_name' => 'Alam',
            'email_id' => 'norr@edoctor.jp',
            'registration_number' => 'ED002',
            'date_of_birth' => '1985-01-15',
            'prefecture_id' => '2',
            'sub_prefecture_id' => '2',
            'visit_start_time' => '16:30:00',
            'visit_end_time' => '18:30:00',
            'speciality_id' => '2',
            'photo' => 'ED002.jpeg',
            'address' => 'Coxs Bazar, Bangladesh',
            'html_code' => '<html><h1>My Name is Noor Alam</h1></html>'
    
        ]);
        $Doctor->save();

        $Doctor = new \App\Doctor([
            'first_name' => 'Arafat',
            'last_name' => 'Ullah',
            'email_id' => 'arafa@edoctor.jp',
            'registration_number' => 'ED003',
            'date_of_birth' => '1988-01-13',
            'prefecture_id' => '3',
            'sub_prefecture_id' => '1',
            'visit_start_time' => '10:30:00',
            'visit_end_time' => '12:30:00',
            'speciality_id' => '3',
            'photo' => 'ED003.jpeg',
            'address' => 'Mirpur, Dhaka, Bangladesh',
            'html_code' => '<html><h1>My Name is Arafat Ullah</h1></html>'
    
        ]);
        $Doctor->save();

        $Doctor = new \App\Doctor([
            'first_name' => 'Rizvee',
            'last_name' => 'San',
            'email_id' => 'rizvee@edoctor.jp',
            'registration_number' => 'ED004',
            'date_of_birth' => '1990-01-23',
            'prefecture_id' => '1',
            'sub_prefecture_id' => '2',
            'visit_start_time' => '12:30:00',
            'visit_end_time' => '14:30:00',
            'speciality_id' => '2',
            'photo' => 'ED004.jpeg',
            'address' => 'Mohammadpur, Dhaka, Bangladesh',
            'html_code' => '<html><h1>My Name is Rizvee</h1></html>'
    
        ]);
        $Doctor->save();
    }
}

// 4	Yonezawa	2
// 3	Shinjo	2
// 5	Nishiusuki	3
// 2	Miyake	1
// 1	Hachijō	1
