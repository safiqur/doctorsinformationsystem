<?php

use Illuminate\Database\Seeder;

class PrefectureTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Prefecture = new \App\Prefecture([
            'prefecture_name' => 'Tokyo'
        ]);
        $Prefecture->save();

        $Prefecture = new \App\Prefecture([
            'prefecture_name' => 'Yamagata'
        ]);
        $Prefecture->save();

        $Prefecture = new \App\Prefecture([
            'prefecture_name' => 'Miyazaki'
        ]);
        $Prefecture->save();
        
    }
}
//Tokyo  Hachijō,Miyake,Ogasawara, Ōshima
//Yamagata Yamagata, Shinjo, Yonezawa and Shonai plains)
//Miyazaki Nishiusuki