<?php

use Illuminate\Database\Seeder;

class DoctorSpecialQualificationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $DoctorSpecialQualification = new \App\DoctorSpecialQualification([
            'doctor_id' => 1,
            'specialqualification_id' => 1
        ]);
        $DoctorSpecialQualification->save();
        //
        $DoctorSpecialQualification = new \App\DoctorSpecialQualification([
            'doctor_id' => 1,
            'specialqualification_id' => 3
        ]);

        $DoctorSpecialQualification->save();

        $DoctorSpecialQualification = new \App\DoctorSpecialQualification([
            'doctor_id' => 2,
            'specialqualification_id' => 2
        ]);
        $DoctorSpecialQualification->save();
        //
        $DoctorSpecialQualification = new \App\DoctorSpecialQualification([
            'doctor_id' => 2,
            'specialqualification_id' => 4
        ]);
        $DoctorSpecialQualification->save();

        $DoctorSpecialQualification = new \App\DoctorSpecialQualification([
            'doctor_id' => 3,
            'specialqualification_id' => 1
        ]);
        $DoctorSpecialQualification->save();
        
        $DoctorSpecialQualification = new \App\DoctorSpecialQualification([
            'doctor_id' => 4,
            'specialqualification_id' => 3
        ]);
        $DoctorSpecialQualification->save();
        
    }
}
