<?php

use Illuminate\Database\Seeder;

class SubPrefectureTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $SPrefecture = new \App\SubPrefecture([
            'subprefecture_name' => 'Hachijō',
             'prefecture_id' => '1'
        ]);
        $SPrefecture->save();

        $SPrefecture = new \App\SubPrefecture([
            'subprefecture_name' => 'Miyake',
            'prefecture_id' => '1'
        ]);
        $SPrefecture->save();

        $SPrefecture = new \App\SubPrefecture([
            'subprefecture_name' => 'Shinjo',
            'prefecture_id' => '2'
        ]);
        $SPrefecture->save();

        $SPrefecture = new \App\SubPrefecture([
            'subprefecture_name' => 'Yonezawa',
            'prefecture_id' => '2'
        ]);
        $SPrefecture->save();

        $SPrefecture = new \App\SubPrefecture([
            'subprefecture_name' => 'Nishiusuki',
            'prefecture_id' => '3'
        ]);
        $SPrefecture->save();
        
    }
}


