<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PrefectureTableSeeder::class);
        $this->call(SubPrefectureTableSeeder::class);
        $this->call(SpecialityTableSeeder::class);
        $this->call(SpecialQualificationTableSeeder::class);
        // $this->call(DoctorTableSeeder::class);
        //$this->call(DoctorSpecialQualificationTableSeeder::class);
        // $this->call(CertificateTableSeeder::class);
        
    }
}
