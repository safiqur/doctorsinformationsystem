<?php

use Illuminate\Database\Seeder;

class SpecialQualificationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //special_qualification
        $SpecialQualification = new \App\SpecialQualification([
            'special_qualification' => 'MD'
        ]);
        $SpecialQualification->save();

        $SpecialQualification = new \App\SpecialQualification([
            'special_qualification' => 'MCM'
        ]);
        $SpecialQualification->save();

        $SpecialQualification = new \App\SpecialQualification([
            'special_qualification' => 'MPhil'
        ]);
        $SpecialQualification->save();

        $SpecialQualification = new \App\SpecialQualification([
            'special_qualification' => 'DMSc'
        ]);
        $SpecialQualification->save();
    }
}

// MD
// MCM
// MPhil
// DMSc