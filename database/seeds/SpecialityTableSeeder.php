<?php

use Illuminate\Database\Seeder;

class SpecialityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $speciality = new \App\Speciality([
            'speciality' => 'Cardiologist'
        ]);
        $speciality->save();

        $speciality = new \App\Speciality([
            'speciality' => 'Nephrologist'
        ]);
        $speciality->save();

        $speciality = new \App\Speciality([
            'speciality' => 'Gastroenterologist'
        ]);
        $speciality->save();
    }
    
}

// Cardiologist
// Nephrologist
// Gastroenterologist