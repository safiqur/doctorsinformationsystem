<?php

use Illuminate\Database\Seeder;

class CertificateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Certificate = new \App\Certificate([
            'doctor_id' => 1,
            'file_name' => 'cert1.pdf',
        ]);
        $Certificate->save();

        $Certificate = new \App\Certificate([
            'doctor_id' => 1,
            'file_name' => 'cert10.pdf',
        ]);
        $Certificate->save();

        $Certificate = new \App\Certificate([
            'doctor_id' => 2,
            'file_name' => 'cert2.pdf',
        ]);
        $Certificate->save();

        $Certificate = new \App\Certificate([
            'doctor_id' => 3,
            'file_name' => 'cert3.pdf',
        ]);
        $Certificate->save();

    }
}
