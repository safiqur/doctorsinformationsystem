<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->increments('id');
            $table->CHAR('first_name');
            $table->CHAR('last_name');
            $table->CHAR('email_id');
            $table->CHAR('registration_number');
            $table->date('date_of_birth');
            $table->smallInteger('prefecture_id');
            $table->smallInteger('sub_prefecture_id');
            $table->time('visit_start_time');
            $table->time('visit_end_time');
            $table->smallInteger('speciality_id');
            $table->CHAR('photo');
            $table->TEXT('address');
            $table->TEXT('html_code');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctors');
    }
}
