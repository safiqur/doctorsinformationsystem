@extends('layouts.master')
@section('pageName')
<h4>Profile View</h4>
@endsection
@section('content')
<!-- 
+First_Name
+Last_Name
+Email
+Registration_Number
DOB
Prefecture
Sub_Prefecture
Visit_Start_Time
Visit_End_Time
SpecialityID
Special_QualificationID
+Photo
+Address
+HTMLCode -->
    
    <div class="row">
        <div class="col-md-12">
            <form action="{{ route('doctorprofilecreate') }}" method="post" enctype="multipart/form-data">                              
          
                <div class="form-group">
                    <label for="firstName">First Name: </label>
                    <label style="color:#d5d5d5;" for="firstName">{{ $doctor->first_name }}</label>
                </div>
                <div class="form-group">
                    <label for="lastName">Last Name: </label>
                    <label for="lastName">{{ $doctor->last_name }}</label>                   
                </div>
                <div class="form-group">
                    <label for="email">Email: </label>
                    <label for="email">{{ $doctor->email_id }}</label>                   
                </div>
                <div class="form-group">
                    <label for="registrationnumber">Registration Number:</label>
                    <label for="registrationnumber">{{ $doctor->registration_number }}</label>
                 </div>
                 <div class="form-group">
                    <label for="">Registration Number:</label>
                    <label for=""></label>
                 </div>
                 <div class="form-group">
                    <label for="">Registration Number:</label>
                    <label for=""></label>
                 </div>
                 <div class="form-group">
                    <label for="">Date Of Birth:</label>
                    <label for="">{{ $doctor->date_of_birth }}</label>
                 </div>
                 <div class="form-group">
                    <label for="">Registration Number:</label>
                    <label for="">{{ $doctor->prefecture->prefecture_name}}</label>
                 </div>
                 <div class="form-group">
                    <label for="">Registration Number:</label>
                    <label for="">{{ $doctor->subprefecture->subprefecture_name }}</label>
                 </div>
                 <div class="form-group">
                    <label for="">Registration Number:</label>
                    <label for="">{{ $doctor->visit_start_time }}</label>
                 </div>
                  <div class="form-group">
                    <label for="">Registration Number:</label>
                    <label for="">{{ $doctor->visit_end_time }}</label>
                 </div>
                 <div class="form-group">
                    <label for="">Registration Number:</label>
                    <label for="">{{ $doctor->speciality->speciality }}</label>
                 </div>
                <label for="photo">Photo</label>  
                <div class="form-group">                 
                 <img id="output"  src="/images/{{ $doctor->photo}}" height="100px" width="150" >
                </div>
                <label for="address">address</label>   
                <div class="form-group">  
                              
                    <textarea id="address" name="address" rows="5" cols="50" disabled="true">                     
                    {{$doctor->address}}
                    </textarea>
                </div> 
                <label for="title">htmlcode</label> 
                <div class="form-group">  
                                
                    <textarea id="html_code" name="htmlcode" rows="5" cols="50" disabled="true">                             
                    {{$doctor->html_code}}
                    </textarea>
                </div> 
                {{ csrf_field() }}
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection