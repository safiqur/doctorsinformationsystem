@extends('layouts.master')
@section('pageName')
<h4>Profile Edit</h4>
@endsection
@section('content')


@if ($errors->any())
<div id="errorSection" class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="row">
    <div class="col-md-12">
        <form action="{{ route('doctorprofileupdate', $id  ) }}" method="post" enctype="multipart/form-data">
            <div id="form">
                <div class="form-group">
                    <label for="firstName">First Name</label>
                    <input type="text" class="form-control" id="first_name" name="first_name" value="{{ $doctor->first_name }}">
                    <div class="form-group">
                        <label for="lastName">Last Name</label>
                        <input type="text" class="form-control" id="last_name" name="last_name" value="{{ $doctor->last_name }}">
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" class="form-control" id="email_id" name="email_id" value="{{ $doctor->email_id }}" disabled>
                    </div>
                    <div class="form-group">
                        <label for="registrationnumber">Registration Number</label>
                        <input type="text" class="form-control" id="registration_number" name="registration_number" value="{{ $doctor->registration_number }}" disabled>
                    </div>
                    <label for="photo">Photo</label>
                    <div class="form-group">
                        <img id="output" src="" height="100px" width="150">
                        <input type="file" id="profilePic" name="profile_pic" accept="image/png, image/jpg,image/jpeg" value={{ $doctor->photo }} onchange="loadFile(event)">
                        <input type="hidden" id="preimageFileName" name="pre-imageFileName" value={{ $doctor->photo }}>


                    </div>

                    <script>
                        var loadFile = function(event) {
                            var fileloader = event.target;
                            var output = document.getElementById('output');
                            output.src = URL.createObjectURL(event.target.files[0]);
                            output.onload = function() {
                                URL.revokeObjectURL(output.src) // free memory
                            }
                        };
                    </script>
                    <label for="Certificate">Certificate(s)</label>
                    <div class="form-group">
                        @foreach($doctor->certificates as $certificate)
                        [<a href="/cert/{{  $certificate->file_name}}" target="_blank">{{ $certificate->file_name}}</a>]
                        @endforeach
                        <input type="file" id="certificates" name="certificates" value={{ $certificate->file_name }} >
                        <input type="hidden" id="precertificate" name="precertificate" value={{ $certificate->file_name }}>
                    </div>

                    <div class="form-group">
                        <input type="date" name="dateofbirth" id="dateofbirth" value={{ Carbon\Carbon::parse($doctor->date_of_birth)->format('Y-m-d H:i:s')}} />
                    </div>
                    <div class="form-group">
                        <input type="time" name="start_time" id="start_time" value={{ Carbon\Carbon::parse($doctor->visit_start_time)->format('H:i')}} />
                    </div>
                    <div class="form-group">
                        <input type="time" name="end_time" id="end_time" value={{ Carbon\Carbon::parse($doctor->visit_end_time)->format('H:i')}} />
                    </div>
                    <label for="prefacture">Prefacture</label>
                    <div class="form-group">

                        <select name="prefacture" id="prefacture">
                            <option value="0">Select Prefecture</option>
                            @foreach($prefectures as $prefecture)
                            <option value="{{ $prefecture->id }}" {{ $doctor->prefecture_id === $prefecture->id ? 'selected' : ''}}>{{ $prefecture->prefecture_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <label for="subprefacture">SubPrefacture</label>
                    <div class="form-group">

                        <select name="subprefacture" id="subprefacture">
                            <option value="0">Select SubPrefecture</option>

                        </select>


                    </div>
                    <div class="form-group">
                        <label for="specialqualifications">Special Qualification(s)</label>
                        @foreach($specialqualifications as $specialqualification)
                        <div class="checkbox">
                            <label id="specialqualifications_{{ $specialqualification->id }}">
                                <input type="checkbox" name="specialqualifications[]" value="{{ $specialqualification->id }}" {{ $doctor->specialQualifications->contains($specialqualification->id) ? 'checked' : '' }}> {{ $specialqualification->special_qualification }}
                            </label>
                        </div>
                        @endforeach
                    </div>
                    <div class="form-group">
                        <label for="speciality">Speciality</label>
                        @foreach($specialities as $specialitie)
                        <div>
                            <input type="radio" id="{{ $specialitie->id }}" name="speciality" value="{{ $specialitie->id }}" {{ $doctor->speciality->id === $specialitie->id  ? 'checked' : '' }}>
                            <label id="speciality_{{ $specialitie->id }}" for="{{ $specialitie->speciality }}">{{ $specialitie->speciality }}</label>
                        </div>
                        @endforeach
                    </div>
                    <label for="address">address</label>
                    <div class="form-group">
                        <textarea id="address" name="address" rows="5" cols="50">
                        {{$doctor->address}}
                        </textarea>
                    </div>
                    <label for="title">htmlcode</label>
                    <div class="form-group">

                        <textarea id="html_code" name="html_code" rows="5" cols="50">
                        {{$doctor->html_code}}
                        </textarea>
                    </div>

                    <button class="preview-button margin-formatter btn-full-width" id="submit" type="button" onclick="preview();">Preview</button>
                    <button class="preview-button margin-formatter btn-full-width" type="submit" onclick>Update</button>

                    <div id="preview" style="display:none;">
                        <div class="form-parent">
                            <div class="form-title">First Name</div>
                            <div id="first_name_preview"></div>
                        </div>
                        <div class="form-parent">
                            <div class="form-title">Last Name</div>
                            <div id="last_name_preview"></div>
                        </div>
                        <div class="form-parent">
                            <div class="form-title">Email</div>
                            <div id="email_id_preview"></div>
                        </div>
                        <div class="form-parent">
                            <div class="form-title">Registraion Number</div>
                            <div id="registration_number_preview"></div>
                        </div>
                        <div class="form-parent">
                            <div class="form-title">Photo</div>
                            <div id="profile_pic_preview"></div>
                        </div>
                        <div class="form-parent">
                            <div class="form-title">Certificate</div>
                            <div id="certificates_preview"></div>
                        </div>
                        <div class="form-parent">
                            <div class="form-title">Date of Birth</div>
                            <div id="dateofbirth_preview"></div>
                        </div>
                        <div class="form-parent">
                            <div class="form-title">Start Time</div>
                            <div id="start_time_preview"></div>
                        </div>
                        <div class="form-parent">
                            <div class="form-title">End time</div>
                            <div id="end_time_preview"></div>
                        </div>
                        <div class="form-parent">
                            <div class="form-title">Prefacture</div>
                            <div id="prefacture_preview"></div>
                        </div>
                        <div class="form-parent">
                            <div class="form-title">Sub-prefacture</div>
                            <div id="subprefacture_preview"></div>
                        </div>

                        <div class="form-parent">
                            <div class="form-title">Special Qualifications</div>
                            <div id="specialqualifications_preview"></div>
                        </div>
                        <div class="form-parent">
                            <div class="form-title">Speciality</div>
                            <div id="speciality_preview"></div>
                        </div>
                        <div class="form-parent">
                            <div class="form-title">Address</div>
                            <div id="address_preview"></div>
                        </div>
                        <div class="form-parent">
                            <div class="form-title">HtmlCode</div>
                            <div id="htmlcode_preview"></div>
                        </div>
                        {{ csrf_field() }}
                        <button class="preview-button margin-formatter btn-full-width" type="submit" onclick>Update</button>
                        <button class="cancel-button btn-full-width ml-20" type="button" onclick="hidePreview()">Back</button>
                    </div>
                </div>
                <!-- END:: preview -->
        </form>
    </div>
</div>
<script>
    function preview() {
        // $errors-> = null;
        var userrange = [];
        var category;

        var firstName = document.getElementById("first_name").value;
        var lastName = document.getElementById("last_name").value;
        var emailid = document.getElementById("email_id").value;
        var dateofbirth = document.getElementById("dateofbirth").value;
        var registrationnumber = document.getElementById("registration_number").value;
        var startime = document.getElementById("start_time").value;
        startime = startime.replace("T", " ");
        var endtime = document.getElementById("end_time").value;
        endtime = endtime.replace("T", " ");
        var dateofbirth = document.getElementById("dateofbirth").value;
        var prefactureSelect = document.getElementById("prefacture");
        var prefacture = prefactureSelect.options[prefactureSelect.selectedIndex].text;
        var subprefactureSelect = document.getElementById("subprefacture");
        let subprefacture = "";
        if (subprefactureSelect.options) {
            subprefacture = subprefactureSelect.options[subprefactureSelect.selectedIndex].text;
        }

        var address = document.getElementById("address").value;
        var htmlcode = document.getElementById("html_code").value;
        // if(document.getElementById('spciality').checked) { 
        //     valspeciality = document.getElementById("spciality").value ;
        //     alert(valspeciality); 
        // }
        let speciality = "";
        var radios = document.getElementsByName('speciality');
        for (i = 0; i < radios.length; i++) {
            if (radios[i].checked) {
                var indx = i + 1;
                speciality = document.getElementById("speciality_" + indx).innerHTML;
            }
        }

        //var specialqualifications = document.getElementById("specialqualifications[]").checked;
        let specialqualifications = "";
        var chkbox = document.getElementsByName('specialqualifications[]');
        for (i = 0; i < chkbox.length; i++) {
            if (chkbox[i].checked) {
                var indx = i + 1;
                //alert(document.getElementById("specialqualifications_"+indx).textContent);
                specialqualifications += "[" + document.getElementById("specialqualifications_" + indx).textContent + "] ";
            }
        }

        var error = 0;


        if (error === 0) {
            document.getElementById("preview").style.display = "block";
            document.getElementById("form").style.display = "none";

            document.getElementById("first_name_preview").innerHTML = firstName;
            document.getElementById("last_name_preview").innerHTML = lastName;
            document.getElementById("email_id_preview").innerHTML = emailid;
            document.getElementById("registration_number_preview").innerHTML = registrationnumber;
            document.getElementById("dateofbirth_preview").innerHTML = dateofbirth;
            document.getElementById("start_time_preview").innerHTML = startime;
            document.getElementById("end_time_preview").innerHTML = endtime;

            // document.getElementById("specialqualifications_preview").innerHTML = specialqualifications;
            document.getElementById("prefacture_preview").innerHTML = prefacture;
            document.getElementById("subprefacture_preview").innerHTML = subprefacture;
            document.getElementById("address_preview").innerHTML = address;
            document.getElementById("htmlcode_preview").innerHTML = htmlcode;
            document.getElementById("speciality_preview").innerHTML = speciality;
            document.getElementById("specialqualifications_preview").innerHTML = specialqualifications;
            alert('error');
        }

    }

    function hidePreview() {
        document.getElementById("preview").style.display = "none";
        document.getElementById("form").style.display = "block";

    }

    $(document).ready(function() {
        $('select[name="prefacture"]').on('change', function() {
            var prefectureID = $(this).val();
            if (prefectureID) {
                $.ajax({
                    url: '/subprefactureByPrefacture/' + prefectureID,
                    type: "GET",
                    dataType: "json",
                    success: function(data) {
                        $('select[name="subprefacture"]').empty();
                        if (data.length < 1)
                            $('select[name="subprefacture"]').append('<option value="0">Select SubPrefecture</option>');
                        $.each(data, function(key, value) {
                            $('select[name="subprefacture"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                    }
                });
            } else {
                $('select[name="subprefacture"]').empty();
            }
        });
    });
</script>

@endsection