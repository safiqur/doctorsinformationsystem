@extends('layouts.master')
@section('pageName')
<h4>Doctor List</h4>
@endsection
@section('content')
@if(Session::has('info'))
        <div class="row">
            <div class="col-md-12">
                <p class="alert alert-info">{{ Session::get('info') }}</p>
            </div>
        </div>
    @endif
<style>
  table,
  th,
  td {
    border: 1px solid black;
    border-collapse: collapse;
    font-weight: bold;
  }

  th,
  td {
    text-align: left;
    padding: 8px;
  }

  tr:nth-child(even) {
    background-color: #D6EEEE;
</style>

<div class="row">
  <div class="col-md-12">
    <form action="{{ route('searchDoctorProfile') }}" method="get" enctype="multipart/form-data">
      <div>
        {{ csrf_field() }}
        <div><button type="submit" class="btn btn-primary" href="{{ route('searchDoctorProfile') }}">Search</button>
          <input type="text" id="searchkeyword" name="searchkeyword" value="" width="200px;">
        </div>
      </div>
      </br>
      <table style="width:100%">
        <tr>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Photo</th>
          <th>Email</th>
          <th>Registration Number</th>
          <th>DOB</th>
          <th>Prefecture</th>
          <th>Sub Prefecture</th>
          <th>Visit Start Time</th>
          <th>Visit End Time</th>
          <th>Speciality</th>
          <th>Qualifications</th>
          <th>Certificates</th>
          <th>Address</th>
          <th>HTMLCode</th>
        </tr>
        @foreach($doctors as $doctor)

        <tr>
          <td>{{ $doctor->first_name }}</td>
          <td>{{ $doctor->last_name }}</td>
          <td> <img id="output" src="/images/{{ $doctor->photo}}" height="50px" width="50"></td>
          <td>{{ $doctor->email_id }}</td>
          <td>{{ $doctor->registration_number }}</td>
          <td>{{ $doctor->date_of_birth }}</td>
          <td>{{ $doctor->prefecture->prefecture_name}}</td>
          <td>{{ $doctor->subprefecture->subprefecture_name }}</td>
          <td>{{ $doctor->visit_start_time }}</td>
          <td>{{ $doctor->visit_end_time }}</td>
          <td>{{ $doctor->speciality->speciality }}</td>
          <td>
            @foreach($doctor->specialQualifications as $qulification)
            [{{ $qulification->special_qualification }}]
            @endforeach
          </td>
          <td>
            @foreach($doctor->certificates as $certificate)
            [<a href="/cert/{{  $certificate->file_name}}" target="_blank">{{ $certificate->file_name}}</a>]

            @endforeach
          </td>
          <td>{{ $doctor->address }}</td>
          <td>{{ strip_tags($doctor->html_code) }} </td>
          <td><a href="{{ route('previewSingleDoctor', ['id' => $doctor->id]) }}">Preview | </a><a href="{{ route('doctorprofileEdit', ['id' => $doctor->id]) }}">Edit | </a><a onclick="return confirm('Confirm Delete?')" href="{{ route('doctorprofiledelete', ['id' => $doctor->id]) }}">Delete</a></td>
          </td>
        </tr>

        @endforeach
       
      </table>


    </form>
  </div>
</div>
@endsection