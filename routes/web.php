<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'uses' => 'DoctorController@welcome',
    'as' => 'welcome'
]);

Route::get('preview/{id}', [
    'uses' => 'DoctorController@previewSingleDoctor',
    'as' => 'previewSingleDoctor'
]);
Route::get('/list', [
    'uses' => 'DoctorController@getDoctorlist',
    'as' => 'doctorprofilelist'
]);

Route::get('create', [
    'uses' => 'DoctorController@CreateDoctorProfile',
    'as' => 'doctorprofilecreate'
]);
Route::post('save', [
    'uses' => 'DoctorController@SaveDoctorProfile',
    'as' => 'doctorprofilesave'
]);


Route::get('edit/{id}', [
    'uses' => 'DoctorController@EditDoctorProfile',
    'as' => 'doctorprofileEdit'
]);

Route::post('update/{id}', [
    'uses' => 'DoctorController@UpdateDoctorProfile',
    'as' => 'doctorprofileupdate'
]);


Route::get('delete/{id}', [
    'uses' => 'DoctorController@DeleteDoctorProfile',
    'as' => 'doctorprofiledelete'
]);

Route::get('subprefactureByPrefacture/{id}', [
    'uses' => 'DoctorController@GetSubprefactureByPrefacture',
    'as' => 'subprefactureByPrefacture'
]);


Route::get('searchDoctorProfile', [
    'uses' => 'DoctorController@searchDoctorProfile',
    'as' => 'searchDoctorProfile'
]);
Auth::routes();

